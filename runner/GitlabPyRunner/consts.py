"""
Constants for gitlab-python-runner
"""
NAME = "gitlab-python-runner"
VERSION = "14.3.21"
USER_AGENT = "{} {}".format(NAME, VERSION)
